﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shawon.Repository.AllRepository
{
    interface ICompanyRepository
    {
        ICollection<CompanyAdd> get();
    }
    public class Company : ICompanyRepository
    {
        private readonly ShawonContext shawonContext;

        public Company(ShawonContext db)
        {
            shawonContext = db;
        }

        ICollection<CompanyAdd> ICompanyRepository.get()
        {
            var companyAdd = shawonContext.CompanyAdd.ToList();
            return companyAdd;
        }
    }

}
